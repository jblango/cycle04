import os
import wmi
import win32con
import win32security
import sys
import win32api
        

def processMonitor(winManInterface):
    print("\n........Process monitoring....\n")
    print("Image Name\tPID\tUser\tExecutatble\tCreation Date\t Timestamp\n")
    print("==========\t===\t====\t============\t===========\t===========\n")
    
    win32ProcessWatchCreation = winManInterface.Win32_Process.watch_for("creation")
      
    while True:

        try:
         
            processWatcher = win32ProcessWatchCreation()
           
            processOwner = processWatcher.GetOwner()
         
            #print(processWatcher.Name,"\t",processWatcher.ProcessId,"\t",processOwner[0],"-- ",\
            #          processOwner[2],"\t", processWatcher.ExecutablePath,"\t",\
            #          processWatcher.CreationDate,"\t",processWatcher.timestamp)
            msg1 = processWatcher.Name + "\t"+ str(processWatcher.ProcessId)
            
            msg2 = "(\tOwner-Unknown \t)"
            try:
                msg2 = "\t(" + processOwner[0] + " [" + processOwner[2] + "])\t"
            except:
                msg2 = "(\tOwner-Unknown \t)"
                
            msg3 = "Unknown Path\t"
            try:
                msg3 = processWatcher.ExecutablePath + "\t"
            except:
                msg3 = "Unknown Path\t"

            msg4 = "Creation Date Unknown\t"
            try:
                msg4 = str(processWatcher.CreationDate) + "\t"
            except:
                msg4 = "Creation Date Unknown\t"

            msg5 = "NO timestamp"
            try:
                msg5 = str(processWatcher.timestamp)
            except:
                msg5 = "NO timestamp"
                
            #msg1 = processWatcher.Name + "\t"+ str(processWatcher.ProcessId) + "\t("+processOwner[0] +"--"
            #msg2 = processOwner[2]+")\t"+ processWatcher.ExecutablePath+ "\t" + str(processWatcher.CreationDate)
  
            msgAll = str(msg1) + str(msg2) + str(msg3) + str(msg4) + str(msg5) 
            print(msgAll)
            logAuditing(msgAll)    
        except KeyboardInterrupt:
            print("\nBreak out -[Creation] \n")
            sys.exit()
        except:
            print("\ncontinue... [Creation] \n")
            pass
        
def listRunningProcess(winInterface):
    print("\n*******************************************************************")
    print(".......Printing running processes.....")
    print("*******************************************************************\n")
    print("Process Name\t Process ID\n")
    for process in winInterface.Win32_Process ():
        msg1 = process.Name + "\t"+ str(process.ProcessId)
        print(msg1)
        logAuditing(msg1)
        
    
    print("*******************************************************************\n")



def logAuditing(msg):
    fd = open("logAuditingCreation.txt","at")
    s = str(msg)
    fd.write(s)
    fd.write("\n")
    fd.close()
    return

def main():
            
    winInterface = wmi.WMI()
    listRunningProcess(winInterface)
    processMonitor(winInterface)

if __name__ == "__main__":
    main()
