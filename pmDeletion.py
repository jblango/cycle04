import os
import wmi
import win32con
import win32security
import sys
import win32api
        

def processMonitor(winManInterface):

    logAuditing("Closed State\tProcess Name\t Process ID \t Process Path \t Time stamp")
    logAuditing("============\t============\t ========== \t ============  \t ===========")
    
    win32ProcessWatchDeletion = winManInterface.Win32_Process.watch_for("deletion")
    
    while True:
   
        try:
         
            processWatcher = win32ProcessWatchDeletion()
           
                   
           
            msg1 = "[Closed]\t" + processWatcher.Name+"\t" + str(processWatcher.ProcessId)+ "\t"

            #print(msg1)
            #logAuditing(msg1)
            
            #logAuditing(str(processWatcher.timestamp))
            #print(processWatcher.timestamp)
            msg2 = "Path Unknown \t"
            try:
                msg2 = processWatcher.ExecutablePath + "\t"
            except:
               msg2 = "Path Unknown \t"
               
            msg3 = str(msg1) + str(msg2) + str(processWatcher.timestamp)
             
            print(msg3)
            logAuditing(msg3)
            
        except KeyboardInterrupt:
            print("Break out - processDeletion")
            sys.exit()
        except:
            print("continue... [processDeletion]")
            pass
        

def logAuditing(msg):
    fd = open("logAuditingDeletion.txt","at")
    s = str(msg)
    fd.write(s)
    fd.write("\n")
    fd.close()
    return

def main():
            
    winInterface = wmi.WMI()
    
    processMonitor(winInterface)

if __name__ == "__main__":
    main()
